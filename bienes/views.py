import logging
import json
from django.http.response import JsonResponse
from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime
from bienes.models import (Verificacion, Bien_Trabajador, Bien_verificacion)


# Create your views here.
def save_proccess_verification(request):
    requestData = json.loads(request.body)
    logging.info(requestData)
    verificacion = Verificacion.objects.get(id=requestData["id_verificacion"])
    # Fecha de verificacion
    fecha_verificacion = datetime.now()

    for data in requestData["lista_bienes"]:
        # id_bien_worked=
        bien_worked = Bien_Trabajador.objects.get(id=data["id"].strip())
        # estado_bien=data["estado"].strip()
        estado_bien = "B"
        observacion = data["obs"].strip()

        try:
            bien_verification = Bien_verificacion.objects.get(
                verificacion=verificacion, bien_trabajador=bien_worked)
            bien_verification.fecha_verificacion = fecha_verificacion
            bien_verification.estado_bien = estado_bien
            bien_verification.save()
            logging.debug("Verification saved")
        except Bien_verificacion.DoesNotExist:
            bien_verification = Bien_verificacion(verificacion=verificacion, bien_trabajador=bien_worked,
                                                  fecha_verificacion=fecha_verificacion, estado_bien=estado_bien, observacion=observacion)
            bien_verification.save()

    return JsonResponse({
        'result': True,
        'message': "Guardado correctamente"
    })
