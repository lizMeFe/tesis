from django.contrib import admin
from easy_select2 import select2_modelform
from bienes.models import (
    Bien_verificacion, Verificacion, Bien_Universidad, Bien_Trabajador)
# par importar y exportar
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from django_admin_listfilter_dropdown.filters import (
    DropdownFilter, ChoiceDropdownFilter, RelatedDropdownFilter
)


# Register your models here.
admin.site.register(Verificacion)
# admin.site.register(Bien_Trabajador)
# admin.site.register(Bien_Universidad)
# admin.site.register(Bien_verificacion)


class Bien_TrabajadorResource(object):
    """docstring for Bientra"""

    class Meta:

        model = Bien_Trabajador

        fields = (
            'bien_trabajador__bien_universidad__DESCRIPCION',
            'bien_trabajador__cargotrabajador__oficina__nombre_oficina',
            'bien_trabajador__cargotrabajador__cargo__nombre_cargo',
            'bien_trabajador__bien_universidad__CODIGO_ACTIVO',
            'bien_trabajador__bien_universidad__DESCRIPCION',

        )
        exclude = ()


class BienResource(resources.ModelResource):

    class Meta:
        model = Bien_Universidad


class BienTrabajadorAdmin(ImportExportModelAdmin):
    """docstring for ClassName"""

    resource_class = Bien_TrabajadorResource

    # list_filter=('bien_trabajador__cargotrabajador__oficina','bien_trabajador__cargotrabajador__cargo',)

    list_display = ('bien_universidad', 'cargotrabajador',
                    'estado_asignacion', 'fecha',)


class BienesUniversidadAdmin(ImportExportModelAdmin):
    """docstring for Bines"""
    resource_class = BienResource
    list_display = ('CODIGO_ACTIVO', 'DESCRIPCION', 'CARACTERISTICAS',
                    'NRO_SERIE', 'MARCA', 'ESTADO_ACTUAL', 'MODELO')


class BienVerificacionResources(resources.ModelResource):
    """docstring for Ver"""
    class Meta:

        model = Bien_verificacion
        # fields=(
        #     'verificacion__nombre_verificacion',
        #     'bien_trabajador__cargotrabajador__oficina__nombre_oficina',
        #     'bien_trabajador__cargotrabajador__cargo__nombre_cargo',
        #     'bien_trabajador__bien_universidad__CODIGO_ACTIVO',
        #     'bien_trabajador__bien_universidad__DESCRIPCION',
        #     'fecha_verificacion',
        #     'estado_bien',

        #     )
        exclude = ()


class BienVerificacionAdmin(ImportExportModelAdmin):
    """docstring for BienVer"""
    resource_class = BienVerificacionResources

    # ('a_charfield', DropdownFilter),
   #      # for choice fields
   #      ('a_choicefield', ChoiceDropdownFilter),
   #      # for related fields
   #      ('a_foreignkey_field', RelatedDropdownFilter),
    list_display = (
        'bien_trabajador',
        'fecha_verificacion',
        'estado_bien',
        'name_verification',
        'getrabajador',
        'getcargo',
    )

    search_fields = (
        'bien_trabajador__cargotrabajador__oficina__nombre_oficina',
    )
    list_filter = ('bien_trabajador__cargotrabajador__cargo__nombre_cargo',
                   'bien_trabajador__cargotrabajador__oficina__nombre_oficina',)


admin.site.register(Bien_Universidad, BienesUniversidadAdmin)
admin.site.register(Bien_Trabajador, BienTrabajadorAdmin)
admin.site.register(Bien_verificacion, BienVerificacionAdmin)
