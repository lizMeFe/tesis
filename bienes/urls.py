from django.urls import path

from . import views

urlpatterns = [
    path('process_verification',
         views.save_proccess_verification, name='guardar_datos'),
]
