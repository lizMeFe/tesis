from django.db import models

# Create your models here.

from trabajadores.models import Cargotrabajador


# Create your models here.
class Bien_Universidad(models.Model):
    """docstring for Bien_Universidad"""
    select_status = (('1', 'Activo Fijo'), ('2', 'Baja'))
    select_type_active = (('1', 'Activo depreciable'),
                          ('2', 'Activo No Depreciable'))
    select_doc_adqui = (('031', ' O/C - Guia --> FECHA_COMPRA,VALOR_COMPRA,NRO_ORDEN'),
                        ('045', 'Nea --> NRO_DOCUMENTO,VALOR_NEA,FECHA_NEA'))
    slect_type_Transnea = [(3, 'NEA-Ingreso Produccion'),
                           (4, 'NEA-Ingreso Donacion'),
                           (5, 'NEA-Transf. Externa'),
                           (8, 'NEA-Diferenc. Inventario'),
                           (9, 'NEA - Otros')]
    slect_flag_item = (('N', 'No'), ('S', 'Si'))
    slect_status_coserv = (('1', 'Bueno'), ('2', 'Regular'), ('3', 'Malo'))

    SEC_EJEC = models.IntegerField()
    TIPO_MODALIDAD = models.IntegerField()
    SECUENCIA = models.IntegerField()
    CODIGO_ACTIVO = models.CharField(max_length=24, unique=True)
    ESTADO = models.CharField(choices=select_status,
                              default='1', max_length=1, null=False)
    TIPO_BIEN = models.CharField(max_length=1)
    GRUPO_BIEN = models.CharField(max_length=2)
    CLASE_BIEN = models.CharField(max_length=2)
    FAMILIA_BIEN = models.CharField(max_length=4)
    ITEM_BIEN = models.CharField(max_length=4)
    DESCRIPCION = models.CharField(max_length=200)
    TIPO_ACTIVO = models.CharField(
        choices=select_type_active, default='1', max_length=1, null=False)
    TIPO_DOC_ADQUISICION = models.CharField(
        choices=select_doc_adqui, default='031', max_length=3)
    TIPO_MOV_NEA = models.CharField(max_length=1)
    TIPO_TRAN_NEA = models.IntegerField(
        choices=slect_type_Transnea, default=3)
    NRO_DOCUMENTO = models.CharField(max_length=10)
    VALOR_NEA = models.DecimalField(decimal_places=6, max_digits=16)
    FECHA_NEA = models.DateField()
    NRO_ORDEN = models.PositiveIntegerField()
    VALOR_COMPRA = models.DecimalField(decimal_places=6, max_digits=16)
    FECHA_COMPRA = models.DateField()
    TIPO_DOC_ALTA = models.CharField(max_length=3)
    NRO_PECOSA = models.IntegerField()
    FECHA_ALTA = models.DateField()
    SEDE = models.IntegerField()
    CENTRO_COSTO = models.CharField(max_length=15)
    EMPLEADO_RESPONSABLE = models.CharField(max_length=15)
    EMPLEADO_FINAL = models.CharField(max_length=15)
    TIPO_UBICAC = models.IntegerField()
    SUBTIPO_UBICAC = models.CharField(max_length=3)
    CARACTERISTICAS = models.CharField(max_length=300, null=True, blank=True)
    MODELO = models.CharField(max_length=40, null=True, blank=True)
    MEDIDAS = models.CharField(max_length=30, null=True, blank=True)
    NRO_SERIE = models.CharField(max_length=20, null=True, blank=True)
    FLAG_ITEM_ESNI = models.CharField(
        choices=slect_flag_item, default='N', max_length=1)
    MARCA = models.DecimalField(decimal_places=0, max_digits=5)
    COD_MODELO = models.DecimalField(
        decimal_places=0, max_digits=3, null=True, blank=True)
    ESTADO_ACTUAL = models.CharField(max_length=1)
    ESTADO_CONSERV = models.CharField(
        choices=slect_status_coserv, default='1', max_length=1)
    FECHA_MOVIMTO = models.DateField()
    PROVEEDOR = models.DecimalField(
        decimal_places=0, max_digits=5, null=True, blank=True)
    COD_ALMACEN = models.CharField(max_length=3)
    SEC_ALMACEN = models.CharField(max_length=3)
    FLAG_ETIQUETA = models.CharField(max_length=1)
    FECHA_ETIQUET = models.DateField(null=True)
    VALOR_INICIAL = models.DecimalField(decimal_places=2, max_digits=16)
    VALOR_DEPREC = models.DecimalField(decimal_places=2, max_digits=16)
    INVENT_SCANER = models.CharField(max_length=1)
    CLASIFICADOR = models.CharField(max_length=20)
    ANIO_EJE = models.DecimalField(decimal_places=0, max_digits=4)
    SUB_CTA = models.CharField(max_length=8)
    MAYOR = models.CharField(max_length=4)
    CODIGO_BARRA = models.CharField(max_length=15, blank=True, null=True)
    OBSERVACIONES = models.CharField(max_length=300, blank=True, null=True)

    class Meta:
        """docstring for Meta"""

        verbose_name_plural = "Bienes Universidad"

    def __str__(self):
        return self.CODIGO_ACTIVO


class Verificacion(models.Model):
    nombre_verificacion = models.CharField(max_length=20)
    documento = models.CharField(max_length=100)
    fecha_inicio = models.DateTimeField()
    fecha_finalizacion = models.DateTimeField()
    estado_verificacion = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = "Verificaciones"

    def __str__(self):
        return self.nombre_verificacion


class Bien_Trabajador(models.Model):
    """docstring for Bien_Trabajador"""
    bien_universidad = models.ForeignKey(
        Bien_Universidad, on_delete=models.PROTECT)
    cargotrabajador = models.ForeignKey(
        Cargotrabajador, on_delete=models.PROTECT)
    fecha = models.DateTimeField()
    estado_asignacion = models.CharField(max_length=30)

    class Meta:
        verbose_name_plural = "Bienes asociados al trabajador"

    def __str__(self):

        return self.cargotrabajador.trabajador.apellidos+"-"+self.bien_universidad.CODIGO_ACTIVO


class Bien_verificacion(models.Model):
    """docstring for Bien_verificacion"""

    verificacion = models.ForeignKey(Verificacion, on_delete=models.PROTECT)
    bien_trabajador = models.ForeignKey(
        Bien_Trabajador, on_delete=models.PROTECT)
    fecha_verificacion = models.DateTimeField()
    estado_bien = models.CharField(max_length=1)
    observacion = models.TextField(blank=True)

    class Meta:
        verbose_name_plural = "Verificación Bienes"

    def __str__(self):

        return str(self.verificacion.id)

    def name_verification(self):
        return self.verificacion.nombre_verificacion

    def getrabajador(self):
        return self.bien_trabajador.cargotrabajador.trabajador.apellidos

    def getcargo(self):
        return self.bien_trabajador.cargotrabajador.cargo.nombre_cargo
