from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from dependencias.models import (Local, Pabellon, Oficina)
from trabajadores.models import (Cargotrabajador)

import json


# Create your views here.

@csrf_exempt
def getLoccals(Request):
    if Request.user.is_authenticated:

        option = Request.POST.get('option')
        qs = []

        if option == "s":
            idsede = Request.POST.get('sede')
            qs = Local.objects.filter(sede=idsede)
            qs_json = serializers.serialize('json', qs)

        if option == "p":
            idlocal = Request.POST.get('local')
            qs = Pabellon.objects.filter(local=idlocal)
            qs_json = serializers.serialize('json', qs)

        if option == "o":
            idpabellon = Request.POST.get('pabellon')
            qs = Oficina.objects.filter(pabellon=idpabellon)
            qs_json = serializers.serialize('json', qs)

        return HttpResponse(qs_json, content_type='application/json')

    # si no hay autorizacion
    qs = []
    qs_json = serializers.serialize('json', qs)
    return HttpResponse(qs_json, content_type='application/json')


@csrf_exempt
def get_trabajadorCargo(Request):

    if Request.user.is_authenticated:

        data = []

        idoficina = Request.POST.get('oficina')
        qs = Cargotrabajador.objects.filter(oficina=idoficina)

        for item in qs:

            trabajador_cargo = {"id": item.id, "trabajador": item.trabajador.apellidos +
                                " "+item.trabajador.nombres, "cargo": item.cargo.nombre_cargo}
            data.append(trabajador_cargo)

        qs_json = json.dumps(data)

        # print(qs_json)

        return HttpResponse(qs_json, content_type='application/json')

    qs = []
    qs_json = serializers.serialize('json', qs)

    return HttpResponse(qs_json, content_type='application/json')
