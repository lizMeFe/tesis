from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from dependencias.models import Sede
from dependencias.models import Local
from dependencias.models import Pabellon
from dependencias.models import Oficina


# Register your models here.
admin.site.register(Sede)
admin.site.register(Local)
admin.site.register(Pabellon)


class OficinaResource(resources.ModelResource):
    class Meta:

        model = Oficina
        exclude = ()


class OficinaAdmin(ImportExportModelAdmin):

    list_display = (
        'nombre_oficina',
        'pabellon',

    )

    list_filter = ('pabellon',)


admin.site.register(Oficina, OficinaAdmin)
