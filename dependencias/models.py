from django.db import models

# Create your models here.


class Sede(models.Model):
    """docstring for Sede"""
    nombre_sede = models.CharField(max_length=50)
    estado_sede = models.BooleanField(default=True)

    def __str__(self):
        return self.nombre_sede


class Local(models.Model):
    """docstring for Dependencia"models.Modelf __init__(self, arg):"""
    nombre_local = models.CharField(max_length=80, unique=True)
    sede = models.ForeignKey(Sede, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Locales"

    def __str__(self):
        return self.nombre_local


class Pabellon(models.Model):
    """docstring for Direccion"""
    nombre_pabellon = models.CharField(max_length=80, unique=True)
    local = models.ForeignKey(Local, on_delete=models.CASCADE)
    estado_pabellon = models.CharField(max_length=30)

    class Meta:
        verbose_name_plural = "Pabellones"

    def __str__(self):
        return self.nombre_pabellon


class Oficina(models.Model):
    """docstring for  Oficina"""
    nombre_oficina = models.CharField(max_length=80, unique=True)
    pabellon = models.ForeignKey(Pabellon, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre_oficina

# class Unidad(models.Model):
#     """docstring for Unidad"""
#     nombre_unidad=models.CharField(max_length=80, unique=True)
#     oficina=models.ForeignKey(Oficina,on_delete=models.CASCADE)
#     def __str__(self):
#         return self.nombre_unidad
