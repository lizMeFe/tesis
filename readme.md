requirements
* Python 3.7 max version
* mysql
* redis
* www.gtk.org/https://weasyprint.readthedocs.io/en/stable/install.html#windows
    https://weasyprint.readthedocs.io/en/stable/install.html#gtk64installer



``` powershell
    pip install -r .\requeriments.txt
    
    python .\manage.py migrate
    
    python manage.py createsuperuser
    
    python .\manage.py runserver 0.0.0.0:8000

#linux

Requirements
- python > 3.4
- Ubuntu(or Linux)
- MySQL Data Base server //https://kwiksteps.com/mysql-ubuntu-on-windows/

Helpers
```
sudo apt-get install python3-pip
```

Setup:

1. Install virtualenv
    ```
    sudo apt install virtualenv
    sudo apt install libmysqlclient-dev
    ```
2. Install virtualenvwrapper
    ```
    sudo pip3 install virtualenv
    sudo pip3 install virtualenvwrapper
    ```
3. Add the following to the end of the .bashrc file
    ``` 
    export WORKON_HOME=~/virtualenvs
    export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python
    source ~/.local/bin/virtualenvwrapper.sh
    ```
4. Execute the .bashrc file
    ```
    source ~/.bashrc
    ```
5. set python version
    ```
    virtualenv -p /usr/bin/python3 venv
    ```
6. create virtual dir
    ```
    mkvirtualenv tesisProject
    //pip uninstall pkg-resources==0.0.0 //line removed from requeriment.txt
    ```
7. make sure you're working in virual dir:
    ```
    workon tesisProject

    //to exit type: deactivate
    ```
8. install dependencies
    ```
    pip3 install -r requeriment.txt
    ```

Run:
```
//pip3 install virtualenvwrapper mkvirtualenv tesisProject workon 
//tesisProject pip3 install -r requeriment.txt

python3 manage.py migrate --noinput && python3 manage.py runserver 0.0.0.0:8000

```

