import logging
from django.shortcuts import render, redirect
#from dependencias.models import Oficina
from trabajadores.models import (Trabajador, Cargotrabajador)
from bienes.models import (Bien_Trabajador, Verificacion)

# Create your views here.


def index(request):
    # return render(request,'rfid/index.html',{})
    logging.info(request.POST)
    idoficina = request.POST.get('office')
    idcargotrabajador = request.POST.get('worked')
    # trabajador=Trabajador.objects.get(pk=idtrabajador)
    # oficina=Oficina.objects.get(pk=idoficina)
    cargotrabajador = Cargotrabajador.objects.get(pk=idcargotrabajador)
    bienes_a_cargo = Bien_Trabajador.objects.filter(
        cargotrabajador=cargotrabajador)
    verificion_programado = Verificacion.objects.filter(
        estado_verificacion=True)

    total_retorno = len(verificion_programado)

    # ultima verificacion programada
    if total_retorno > 0:
        # recuperando primer elemento...que representa a la ultima programacion
        verificion_programado = verificion_programado[0]

    return render(request, 'rfid/view_bienes.html',
                  {'room_name': "read",
                   'bienes_data': bienes_a_cargo,
                   'verificacion': verificion_programado,
                   'estado': total_retorno})

    # return redirect('room','read')


def room(request, room_name):

    idoficina = request.POST.get('office')
    idtrabajador = request.POST.get('worked')
    print(request.POST)

    print(str(idoficina)+"->"+str(idtrabajador))

    # return render (request,'rfid/room.html',
    return render(request, 'rfid/view_bienes.html',
                  {'room_name': room_name})
