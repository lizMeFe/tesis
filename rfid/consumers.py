from logging import log
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
import json
import logging


class RfidConsumer(WebsocketConsumer):
    """docstring for   RfidConsumer"""

    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'rfid_%s' % self.room_name

        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name)
        self.accept()

    def disconnect(self, colse_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        logging.debug("data received")
        logging.warning(text_data)
        logging.warning(text_data_json)
        message = text_data_json
        # message = text_data_json['message']

        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'rfid_message',
                'message': message
            }
        )

    def rfid_message(self, event):

        message = event['message']

        self.send(text_data=json.dumps({
            'message': message
        }))
