from django.db import models
from dependencias.models import Oficina

# Create your models here.


class Trabajador(models.Model):
    nro_documento = models.CharField(max_length=8, unique=True)
    apellidos = models.CharField(max_length=100)
    nombres = models.CharField(max_length=80)

    class Meta:
        verbose_name_plural = "Trabajadores"

    def __str__(self):
        return self.apellidos+" "+self.nombres


class Cargo(models.Model):
    """docstring for Cargo"""
    nombre_cargo = models.CharField(
        max_length=60, blank=False, default='Sin Cargo')
    cargotrabajo = models.ManyToManyField(
        Trabajador, through="Cargotrabajador")

    def __str__(self):
        return self.nombre_cargo


class Cargotrabajador(models.Model):
    """docstring for ClassName"""
    trabajador = models.ForeignKey(Trabajador, on_delete=models.CASCADE)
    cargo = models.ForeignKey(Cargo, on_delete=models.CASCADE)
    oficina = models.ForeignKey(Oficina, on_delete=models.CASCADE)
    docu_asignacion = models.CharField(max_length=100)
    fecha_registro = models.DateTimeField(auto_now_add=True)
    fecha_cese = models.DateTimeField()
    ducu_cese = models.CharField(max_length=100)
    estado_cargo = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = "Cargos del trabajador"

    def __str__(self):
        return self.trabajador.apellidos + " "+self.cargo.nombre_cargo
