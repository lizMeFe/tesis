from django.contrib import admin

# Register your models here.
from trabajadores.models import Trabajador
from trabajadores.models import Cargo
from trabajadores.models import Cargotrabajador

admin.site.register(Trabajador)
admin.site.register(Cargo)
admin.site.register(Cargotrabajador)