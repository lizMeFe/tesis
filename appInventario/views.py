from django.http import request
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from dependencias.models import Sede, Local, Pabellon, Oficina

# Create your views here.


def index(request):

    print("llegamos a index")

    if request.user.is_authenticated:

        sedes = Sede.objects.all()

        context = {'sedes': sedes}

        return render(request, 'index.html', context)

    return redirect('login')


def loginuser(request):

    if request.method == 'POST':

        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        print("proceso de autenticado")

        if user:
            login(request, user)
            print("redirigiendo a index rolandoo")
            return redirect('index')

    if request.user.is_authenticated:

        return redirect('index')

    return render(request, 'users/login.html', {})


def logoutuser(request):

    logout(request)
    # Redireccionamos a la portada
    return redirect('/login')


def rfid1(request):

    return redirect('rfid/')
