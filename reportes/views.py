from datetime import datetime
from django.template import RequestContext
from django.template.base import logger
from django.template.loader import get_template
from trabajadores.models import Cargotrabajador
from bienes.models import Bien_Trabajador, Verificacion
from django.http import JsonResponse, request
from django.shortcuts import render
from django.http import HttpResponse
from weasyprint import CSS, HTML
from django.conf import settings
import json
import logging

# Create your views here.


def bienes(request):

    data = []
    qs = Cargotrabajador.objects.all()

    for item in qs:
        trabajador_cargo = {"id": item.id,
                            "nombres": item.trabajador.apellidos + " "+item.trabajador.nombres,
                            "cargo": item.cargo.nombre_cargo}
        data.append(trabajador_cargo)

    return render(request, 'reportes/bienes.html', {
        'trabajadores_cargo': data
    })


def bienes_trabajador(request, idcargotrabajador):

    cargotrabajador = Cargotrabajador.objects.get(pk=idcargotrabajador)
    bienes_a_cargo = Bien_Trabajador.objects.filter(
        cargotrabajador=cargotrabajador)

    data = []

    for item in bienes_a_cargo:

        bien = {
            'id': item.id,
            'CODIGO_ACTIVO': item.bien_universidad.CODIGO_ACTIVO,
            'ITEM_BIEN': item.bien_universidad.ITEM_BIEN,
            'TIPO_BIEN': item.bien_universidad.TIPO_BIEN,
            'DESCRIPCION': item.bien_universidad.DESCRIPCION,
            'FECHA_COMPRA': item.bien_universidad.FECHA_COMPRA,
            'ESTADO_ACTUAL': item.bien_universidad.ESTADO_ACTUAL,
            'VALOR_INICIAL': item.bien_universidad.VALOR_INICIAL,
            'EMPLEADO_RESPONSABLE': item.bien_universidad.EMPLEADO_RESPONSABLE}
        data.append(bien)

    return JsonResponse({
        'bienes': data
    })


def reporte_descargar(request, idcargotrabajador):
    """ Generate pdf. """
    cargotrabajador = Cargotrabajador.objects.get(pk=idcargotrabajador)
    bienes_a_cargo = Bien_Trabajador.objects.filter(
        cargotrabajador=cargotrabajador)

    template = get_template('reportes/pdfBienes.html')
    html = template.render({
        'verificacion_fecha': datetime.now(),
        'bienes': bienes_a_cargo
    })
    root_url = request.build_absolute_uri('/')[:-1].strip("/")
    cssPage = CSS(root_url+"/static/css/styles.css")
    pdfStyles = CSS(root_url+"/static/css/pdfStyle.css")
    response = HttpResponse(content_type='application/pdf')
    HTML(string=html, base_url=request.build_absolute_uri()).write_pdf(response, stylesheets=[
        cssPage,
        pdfStyles])

    # presentational_hints=True

    return response


def previsualizacion_reporte(request, idcargotrabajador):
    """ Generate pdf. """
    cargotrabajador = Cargotrabajador.objects.get(pk=idcargotrabajador)
    bienes_a_cargo = Bien_Trabajador.objects.filter(
        cargotrabajador=cargotrabajador)

    return render(request, 'reportes/pdfBienes.html', {
        'idcargotrabajador': idcargotrabajador,
        'verificacion_fecha': datetime.now(),
        'bienes': bienes_a_cargo
    })
