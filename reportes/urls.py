from django.urls import path

from . import views

urlpatterns = [
    path('bienes', views.bienes, name='bienes'),
    path('bienes/<str:idcargotrabajador>',
         views.bienes_trabajador, name='bienes del trabajador'),
    path('bienes/<str:idcargotrabajador>/pdf',
         views.reporte_descargar, name='descargar-reporte'),
    path('bienes-preview/<str:idcargotrabajador>',
         views.previsualizacion_reporte, name='preview-reporte')
]
